module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"
  key_name   = "key"
  public_key = "${var.ec2_sshkey}"
}

data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_security_group" "server" {
  vpc_id      = aws_vpc.Main.id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

}


resource "aws_instance" "server" {
  ami                         = data.aws_ami.amazon-linux-2.id
  instance_type               = "t2.micro"
  key_name                    = module.key_pair.key_pair_key_name
  subnet_id                   = aws_subnet.public_subnets.id
  vpc_security_group_ids      = [aws_security_group.server.id]
  associate_public_ip_address = true
  user_data                   = <<-EOF
                                #!/bin/bash
                                yum -y install httpd
                                sudo systemctl enable httpd
                                sudo systemctl start httpd
                                EOF
}

output "DNS" {
  value = aws_instance.server.public_ip
}
